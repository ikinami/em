let texts = [];
const transformTextPosition = [,7,8,9,4,5,6,1,2,3];

const init = () => {
  let textInputs = document.getElementById('textinputs');
  for (let i = 1; i < 10; i++) {
    let inputElem = document.createElement('input');
    texts[transformTextPosition[i]] = inputElem;
    inputElem.onchange = rerender;
    textInputs.appendChild(inputElem);
  }
  rerender();
};

const rerender = () => {
  let params = {};
  ['i', 'bg', 't', 'c'].forEach(property => {
    params[property] = document.getElementById(property).value;
  });
  for (let i = 1; i < 10; i++) {
    params[`t${i}`] = texts[i].value;
  }
  if (params.bg == '#000000') {
    params.bg = undefined;
  }
  if (params.c == '#ffffff') {
    params.c = undefined;
  }
  const link = `https://ikinami.gitlab.io/em/?${Object
    .entries(params)
    .filter(([prop, val]) => val)
    .map(([prop, val]) => `${prop}=${val}`)
    .join('&')}`;
  const linkElem = document.getElementById('link');
  linkElem.innerText = link;
  linkElem.href = link;
  renderPage(params);
};
