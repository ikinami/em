const renderPage = (urlParams) => {
  
  const img = urlParams.image || urlParams.i;
  if (img) {
    document.getElementById('image').src = img;
  }
  
  const username = urlParams.username || urlParams.u;
  if (username) {
    urlParams.text2 = username;
  }
  
  const background = urlParams.bg || urlParams.background;
  if (background) {
    document.getElementById('display').style.background = background;
  }
  
  const display = document.getElementById('display');
  let leftoverTexts = [...document.getElementsByClassName('text')];
  leftoverTexts.forEach(text => text.remove());
  for (let i = 0; i < 10; i++) {
    let text = urlParams[`t${i}`] || urlParams[`text${i}`];
    if (text) {
      let elem = document.createElement('div');
      elem.classList.add('text', 'bordered');
      elem.style['grid-area'] = `${3-Math.floor(i/3-0.1)} / ${(i%3 || 3)}`;
      if (i == 0) elem.style['grid-area'] = '1 / 1 / span 3 / span 3';
      elem.innerText = text;
      display.appendChild(elem);
      let color = urlParams[`c${i}`]
        || urlParams[`color${i}`]
        || urlParams.c;
      if (color) {
        elem.style.color = color;
      }
    }
  }
};
