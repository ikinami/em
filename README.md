# Emote Editor 🔨

Emote Editor is a way to easily create custom links for emotive messages with captions.
It is a small webdev project written in vanilla Javascript, HTML5 and CSS.
Ultra responsive thanks to CSS Grid 😊.

In order to access the editor, go to https://ikinami.gitlab.io/em/editor.

Supported functionality:
* adding a custom image for your background by changing the image link
* changing the text and the background colors by editing the respective fields
* specifying the title of the tab and window in the title filed
* Specifying the texts in nine locations tied to nine positions that responsively adapt to the image
* Copy or click the link at the bottom, to get the link to your new emote

Currently only the use of the middle text fields is recommended, as the image is resized to fit displays, so the sides may be cut off.
The Emote Editor will do its best to fit everything into the new viewport, but on extreme aspect ratios (e.g. portrait displays) it will break in unexpected ways.

Made with ❤️  by [Kinami Imai 今井きなみ](https://ikinami.gitlab.io/kinami/)
